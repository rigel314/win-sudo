//go:build windows

/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// feedback.go: elevated logic fallback when conpty is unavailable

package main

// func fallback() error {
// 	// program := flag.Args()
// 	// log.Println(program)

// 	// if len(program) < 1 {
// 	// 	log.Fatal("no program")
// 	// }

// 	// cmd := exec.Command(program[0], program[1:]...)

// 	// cmd.Stdin = os.Stdin
// 	// cmd.Stdout = os.Stdout
// 	// cmd.Stderr = os.Stderr

// 	// err := cmd.Start()
// 	// if err != nil {
// 	// 	log.Println("can't start program,", err)
// 	// 	return
// 	// }

// 	// cmd.Wait()

// 	return nil
// }
