//go:build windows

/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// unelevated.go: unelevated logic to serve pipes and connect them to the host console

package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Microsoft/go-winio"
	"golang.org/x/sys/windows"
)

func unelevated(options args) (int, error) {
	// register for sigint to handle Ctrl-C
	incomingSignalCh := make(chan os.Signal, 1)
	signal.Notify(incomingSignalCh, os.Interrupt)

	// TODO: handle Ctrl-Break

	// setup named pipes for handling all traffic, stdin(unelevated->elevated), stdout(elevated->unelevated), out-of-band(unelevated->elevated), out-of-band(elevated->unelevated)
	if options.Verbose {
		log.Println("setting up pipe listeners")
	}
	pid := os.Getpid()
	lstdin, err := winio.ListenPipe(fmt.Sprintf("%s%s.%d", piperoot, "lstdin", pid), nil)
	if err != nil {
		return -1, fmt.Errorf("failed ListenPipe for stdin pipe, %w", err)
	}
	defer lstdin.Close()
	lstdout, err := winio.ListenPipe(fmt.Sprintf("%s%s.%d", piperoot, "lstdout", pid), nil)
	if err != nil {
		return -1, fmt.Errorf("failed ListenPipe for stdout pipe, %w", err)
	}
	defer lstdout.Close()
	loobToElevated, err := winio.ListenPipe(fmt.Sprintf("%s%s.%d", piperoot, "loobToElevated", pid), nil)
	if err != nil {
		return -1, fmt.Errorf("failed ListenPipe for oobToElevated pipe, %w", err)
	}
	defer loobToElevated.Close()
	loobFromElevated, err := winio.ListenPipe(fmt.Sprintf("%s%s.%d", piperoot, "loobFromElevated", pid), nil)
	if err != nil {
		return -1, fmt.Errorf("failed ListenPipe for oobFromElevated pipe, %w", err)
	}
	defer loobFromElevated.Close()

	// TODO: also handle stderr, and maybe other magic stuff for powershell

	// make terminal raw
	if options.Verbose {
		log.Println("making terminal raw")
	}
	// mode, err := term.MakeRaw(int(os.Stdin.Fd()))
	// if err != nil {
	// 	return -1, fmt.Errorf("failed MakeRaw, %w", err)
	// }
	// defer term.Restore(int(os.Stdin.Fd()), mode)
	mode, err := makeRaw(int(os.Stdin.Fd()), int(os.Stdout.Fd()))
	if err != nil {
		return -1, fmt.Errorf("failed MakeRaw, %w", err)
	}
	defer mode.restore()

	// run a handler for each pipe
	if options.Verbose {
		log.Println("launching pipe handlers")
	}
	go func() { // stdin handler
		// wait for a connection, then copy from stdin to the connection forever
		c, err := lstdin.Accept()
		if err != nil {
			log.Println(fmt.Errorf("failed Accept for stdin pipe, %w", err))
			return
		}
		defer c.Close()
		var ird io.Reader = os.Stdin
		if options.LogRawIn {
			// optionally, copy reads of stdin to a hex logger
			ird = io.TeeReader(os.Stdin, unelevatedHexLogger("raw stdin"))
		}
		for { // ignore io.EOF from ctrl-break
			_, err = io.Copy(c, ird)
			if err != nil {
				log.Println(fmt.Errorf("failed Copy for stdin pipe, %w", err))
				return
			}
		}
	}()
	go func() { // stdout handler
		// wait for a connection, then copy from the connection to stdout forever
		c, err := lstdout.Accept()
		if err != nil {
			log.Println(fmt.Errorf("failed Accept for stdout pipe, %w", err))
			return
		}
		defer c.Close()
		var iwr io.Writer = os.Stdout
		if options.LogRawOut {
			// optionally, duplicate stdout writes to a hex logger
			iwr = io.MultiWriter(os.Stdout, unelevatedHexLogger("raw stdout"))
		}
		_, err = io.Copy(iwr, c)
		if err != nil {
			log.Println(fmt.Errorf("failed Copy for stdout pipe, %w", err))
			return
		}
		// log.Println("stdout done")
	}()
	go func() { // oob to elevated handler
		// wait for a connection, then send out-of-band commands/signals/etc to elevated
		c, err := loobToElevated.Accept()
		if err != nil {
			log.Println(fmt.Errorf("failed Accept for oob to elevated pipe, %w", err))
			return
		}
		defer c.Close()
		gwr := gob.NewEncoder(c)
		// send command+environment to elevated
		err = gwr.Encode(wrap{V: options})
		if err != nil {
			log.Println(fmt.Errorf("failed Encode for oob to elevated pipe, %w", err))
			return
		}
		go func() { // console size check
			var info windows.ConsoleScreenBufferInfo
			var lastX, lastY int
			for {
				// once a second, check for changes to the unelvated console size, if it's different send an update to elevated
				time.Sleep(time.Second)
				h, err := windows.GetStdHandle(windows.STD_OUTPUT_HANDLE)
				if err != nil {
					log.Println("failed GetStdHandle, ", err)
					return
				}
				err = windows.GetConsoleScreenBufferInfo(h, &info)
				if err != nil {
					log.Println("failed GetConsoleScreenBufferInfo, ", err)
					return
				}
				x, y := int(info.Window.Right-info.Window.Left+1), int(info.Window.Bottom-info.Window.Top+1)
				if x != lastX || y != lastY {
					log.Println(x, y)
					lastX = x
					lastY = y
					err := gwr.Encode(wrap{V: conSize{x, y}})
					if err != nil {
						log.Println("failed Encode for conSize", err)
					}
				}
			}
		}()
		// Ctrl-C check
		for v := range incomingSignalCh {
			// notify elevated that a Ctrl-C happened
			s, ok := v.(syscall.Signal)
			if !ok {
				log.Printf("Got signal that wasn't a syscall.Signal, %T\n", v)
				continue
			}
			// log.Println("got sig")
			err = gwr.Encode(wrap{V: sig{Signal: s}})
			if err != nil {
				log.Println(fmt.Errorf("failed Encode for oob to elevated pipe, %w", err))
				return
			}
		}
	}()

	// oob from elevated handler
	// wait for a connection, then handle exit/log events from elevated
	c, err := loobFromElevated.Accept()
	if err != nil {
		return -1, fmt.Errorf("failed Accept for oob from elevated pipe, %w", err)
	}
	defer c.Close()
	grd := gob.NewDecoder(c)
	var w wrap
oobFromElevatedLoop:
	for {
		err = grd.Decode(&w)
		if err != nil {
			return -1, fmt.Errorf("failed Decode for oob from elevated pipe, %w", err)
		}
		switch v := w.V.(type) {
		case exit:
			if options.Verbose {
				log.Printf("Process exited: %+v\n", v)
			}
			break oobFromElevatedLoop
		case elevatedLog:
			v.Log = bytes.TrimRight(v.Log, "\n\r")
			log.Println("[elevated] " + string(v.Log))
			continue
		}
	}

	// sleep for a second to allow any extra stdout to make it to unelevated
	time.Sleep(time.Second)
	// log.Println(hex.EncodeToString(bout.Bytes()))

	// return status from elevated
	return w.V.(exit).Status, nil
}
