//go:build windows

/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// util.go: various helper functions for interacting with windows, supplemental to packages x/sys/windows and syscall

package main

import (
	"fmt"
	"os"
	"syscall"
	"unicode/utf16"
	"unsafe"

	"golang.org/x/sys/windows"
)

var (
	kernel32, _                          = syscall.LoadLibrary("kernel32.dll")
	createPseudoConsole, _               = syscall.GetProcAddress(kernel32, "CreatePseudoConsole")
	resizePseudoConsole, _               = syscall.GetProcAddress(kernel32, "ResizePseudoConsole")
	closePseudoConsole, _                = syscall.GetProcAddress(kernel32, "ClosePseudoConsole")
	initializeProcThreadAttributeList, _ = syscall.GetProcAddress(kernel32, "InitializeProcThreadAttributeList")
	updateProcThreadAttribute, _         = syscall.GetProcAddress(kernel32, "UpdateProcThreadAttribute")
	createProcessW, _                    = syscall.GetProcAddress(kernel32, "CreateProcessW")
	getConsoleWindow, _                  = syscall.GetProcAddress(kernel32, "GetConsoleWindow")
	generateConsoleCtrlEvent, _          = syscall.GetProcAddress(kernel32, "GenerateConsoleCtrlEvent")

	user32, _     = syscall.LoadLibrary("user32.dll")
	showWindow, _ = syscall.GetProcAddress(user32, "ShowWindow")
)

func shellExecute(verb string) error {
	// verb := "runas"
	exe, err := os.Executable()
	if err != nil {
		return err
	}
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	args := fmt.Sprintf("-unelevatedPid %d", os.Getpid())

	verbPtr, err := syscall.UTF16PtrFromString(verb)
	if err != nil {
		return err
	}
	exePtr, err := syscall.UTF16PtrFromString(exe)
	if err != nil {
		return err
	}
	cwdPtr, err := syscall.UTF16PtrFromString(cwd)
	if err != nil {
		return err
	}
	argPtr, err := syscall.UTF16PtrFromString(args)
	if err != nil {
		return err
	}

	var showCmd int32 = windows.SW_NORMAL

	err = windows.ShellExecute(0, verbPtr, exePtr, argPtr, cwdPtr, showCmd)
	if err != nil {
		return err
	}

	return nil
}

func coordToUintptr(x, y uint16) (ret uintptr) {
	return uintptr(x) | uintptr(y)<<16
}

func createProcess(appName *uint16, commandLine *uint16, procSecurity *windows.SecurityAttributes, threadSecurity *windows.SecurityAttributes, inheritHandles bool, creationFlags uint32, env *uint16, currentDir *uint16, startupInfo *startupInfoEx, outProcInfo *windows.ProcessInformation) (err error) {
	var _p0 uint32
	if inheritHandles {
		_p0 = 1
	} else {
		_p0 = 0
	}
	r1, _, e1 := syscall.SyscallN(createProcessW, uintptr(unsafe.Pointer(appName)), uintptr(unsafe.Pointer(commandLine)), uintptr(unsafe.Pointer(procSecurity)), uintptr(unsafe.Pointer(threadSecurity)), uintptr(_p0), uintptr(creationFlags), uintptr(unsafe.Pointer(env)), uintptr(unsafe.Pointer(currentDir)), uintptr(unsafe.Pointer(startupInfo)), uintptr(unsafe.Pointer(outProcInfo)))
	if r1 == 0 {
		if e1 != 0 {
			err = e1
		} else {
			err = syscall.EINVAL
		}
	}
	return
}

type startupInfoEx struct {
	windows.StartupInfo
	lpAttributeList *byte
}

const (
	PROC_THREAD_ATTRIBUTE_NUMBER   = 0x0000FFFF
	PROC_THREAD_ATTRIBUTE_THREAD   = 0x00010000 // Attribute may be used with thread creation
	PROC_THREAD_ATTRIBUTE_INPUT    = 0x00020000 // Attribute is input only
	PROC_THREAD_ATTRIBUTE_ADDITIVE = 0x00040000 // Attribute may be "accumulated," e.g. bitmasks, counters, etc.

	ProcThreadAttributePseudoConsole = 22
)

func procThreadAttributeValue(number uintptr, thread, input, additive bool) uintptr {
	var t uintptr
	var i uintptr
	var a uintptr
	if thread {
		t = PROC_THREAD_ATTRIBUTE_THREAD
	}
	if input {
		i = PROC_THREAD_ATTRIBUTE_INPUT
	}
	if additive {
		a = PROC_THREAD_ATTRIBUTE_ADDITIVE
	}
	return number&PROC_THREAD_ATTRIBUTE_NUMBER | t | i | a
}

// (((Number) & PROC_THREAD_ATTRIBUTE_NUMBER) |
// ((Thread != FALSE) ? PROC_THREAD_ATTRIBUTE_THREAD : 0) |
// ((Input != FALSE) ? PROC_THREAD_ATTRIBUTE_INPUT : 0) |
// ((Additive != FALSE) ? PROC_THREAD_ATTRIBUTE_ADDITIVE : 0))

var PROC_THREAD_ATTRIBUTE_PSEUDOCONSOLE = procThreadAttributeValue(ProcThreadAttributePseudoConsole, false, true, false)

// Custom versions of terminal.MakeRaw and terminal.Restore for testing
type termState struct {
	stdinmode, stdoutmode uint32
	stdin, stdout         int
}

// makeRaw put the terminal connected to the given file descriptor into raw
// mode with VT processing and returns the previous state of the terminal so
// that it can be restored.
func makeRaw(stdin, stdout int) (*termState, error) {
	var stdinmode uint32
	if err := windows.GetConsoleMode(windows.Handle(stdin), &stdinmode); err != nil {
		return nil, err
	}
	stdinraw := stdinmode &^ (windows.ENABLE_ECHO_INPUT | windows.ENABLE_PROCESSED_INPUT | windows.ENABLE_LINE_INPUT | windows.ENABLE_PROCESSED_OUTPUT)
	stdinraw = stdinraw | windows.ENABLE_VIRTUAL_TERMINAL_INPUT
	if err := windows.SetConsoleMode(windows.Handle(stdin), stdinraw); err != nil {
		return nil, err
	}

	var stdoutmode uint32
	if err := windows.GetConsoleMode(windows.Handle(stdout), &stdoutmode); err != nil {
		windows.SetConsoleMode(windows.Handle(stdin), stdinmode)
		return nil, err
	}
	// stdoutraw := stdoutmode &^ (windows.ENABLE_ECHO_INPUT | windows.ENABLE_PROCESSED_INPUT | windows.ENABLE_LINE_INPUT | windows.ENABLE_PROCESSED_OUTPUT)
	stdoutraw := stdoutmode
	stdoutraw = stdoutraw | windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING
	if err := windows.SetConsoleMode(windows.Handle(stdout), stdoutraw); err != nil {
		windows.SetConsoleMode(windows.Handle(stdin), stdinmode)
		return nil, err
	}

	return &termState{stdinmode, stdoutmode, stdin, stdout}, nil
}

// restore restores the terminal connected to the given file descriptor to a
// previous state.
func (state *termState) restore() error {
	windows.SetConsoleMode(windows.Handle(state.stdin), state.stdinmode)
	windows.SetConsoleMode(windows.Handle(state.stdout), state.stdoutmode)
	return nil
}

/*
A pointer to the environment block for the new process. If this parameter is NULL, the new process uses the environment of the calling process.

An environment block consists of a null-terminated block of null-terminated strings. Each string is in the following form:

name=value\0

Because the equal sign is used as a separator, it must not be used in the name of an environment variable.

An environment block can contain either Unicode or ANSI characters. If the environment block pointed to by lpEnvironment contains Unicode characters, be sure that dwCreationFlags includes CREATE_UNICODE_ENVIRONMENT. If this parameter is NULL and the environment block of the parent process contains Unicode characters, you must also ensure that dwCreationFlags includes CREATE_UNICODE_ENVIRONMENT.

The ANSI version of this function, CreateProcessA fails if the total size of the environment block for the process exceeds 32,767 characters.

Note that an ANSI environment block is terminated by two zero bytes: one for the last string, one more to terminate the block. A Unicode environment block is terminated by four zero bytes: two for the last string, two more to terminate the block.
*/
func stringsToEnvironUTF16Ptr(env []string) *uint16 {
	var out []uint16
	for _, s := range env {
		// TODO: care about embedded NUL checking
		out = append(out, utf16.Encode([]rune(s+"\x00"))...)
	}

	out = append(out, utf16.Encode([]rune("\x00\x00"))...)

	return &out[0]
}

// sendCtrlBreak sends a Ctrl-Break event to the process with id pid
func sendCtrlBreak(pid int) error {
	ret, _, errno := syscall.SyscallN(generateConsoleCtrlEvent, uintptr(syscall.CTRL_BREAK_EVENT), uintptr(pid))
	if ret == 0 {
		return errno // TODO: maybe syscall.GetLastError() here?
	}
	return nil
}
