module gitlab.com/rigel314/win-sudo

go 1.19

require (
	github.com/Microsoft/go-winio v0.6.0
	golang.org/x/sys v0.2.0
)

require (
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/tools v0.3.0 // indirect
)
