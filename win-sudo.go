//go:build windows

/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// win-sudo.go: entry point and unelvated<->elevated message definitions

package main

import (
	"encoding/gob"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"syscall"

	"github.com/Microsoft/go-winio"
)

var GIT_VER = "Uncontrolled"

var unelevatedPid = flag.Int("unelevatedPid", -1, "internal, pass to prevent launching self as admin before exec, and to be a pipe client")
var shellexecuteVerb = flag.String("shellexecute-verb", "runas", "debug change to 'open' to prevent launching as admin, but still do everything else the same - useful for testing wsl terminal codes")

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// argument parsing
	var options args
	flag.BoolVar(&options.Verbose, "verbose", false, "print verbose info")
	flag.StringVar(&options.Logfile, "logfile", "", "when present, log debug info to a file, most useful with -verbose")
	flag.BoolVar(&options.LogRawOut, "log-raw-out", false, "log raw output")
	flag.BoolVar(&options.LogRawIn, "log-raw-in", false, "log raw input, this will log passwords")
	flag.BoolVar(&options.DontHideElevated, "dont-hide-elevated", false, "pass to prevent hiding the elevated console")
	flag.BoolVar(&options.HangElevated, "hang-elevated", false, "pass to prevent the elevated console from returning automatically")

	flag.Parse()

	options.Args = flag.Args()
	options.Env = os.Environ()

	if options.Logfile != "" {
		f, err := os.OpenFile(options.Logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println("failed OpenFile, (", options.Logfile, "),", err)
		} else {
			log.SetOutput(f)
		}
	}

	log.Println("Version:", GIT_VER)

	dir, err := os.Getwd()
	if err != nil {
		log.Println("failed Getwd, using default")
	}
	absdir, err := filepath.Abs(dir)
	if err != nil {
		log.Println("failed Abs, using default cwd")
	}
	options.Cwd = absdir

	// unelevated path, this is called first and relaunches this binary (with different args) elevated
	if *unelevatedPid < 0 {
		// launch elevated win-sudo
		err := shellExecute(*shellexecuteVerb)
		if err != nil {
			log.Println("failed shellExecute,", err)
			return
		}

		// run the unelevated handling goroutines
		ret, err := unelevated(options)
		if err != nil {
			log.Println("failed unelevated,", err)
		}
		os.Exit(ret)
		return
	}

	// elevated path
	pid := *unelevatedPid

	// connect named pipe for oob(elevated->unelevated)
	coobFromElevated, err := winio.DialPipe(fmt.Sprintf("%s%s.%d", piperoot, "loobFromElevated", pid), nil)
	if err != nil {
		log.Println(fmt.Errorf("failed DialPipe for oobFromElevated, %w", err))
	}
	defer coobFromElevated.Close()

	gwr := gob.NewEncoder(coobFromElevated)

	log.SetFlags(log.Lshortfile)
	// send logs to stdout and through pipe
	log.SetOutput(io.MultiWriter(os.Stdout, elevatedLogger(gwr)))

	var exitStatus exit
	// launch the child process and run the elevated handling goroutines
	ret, err := elevated()
	if err != nil {
		switch v := err.(type) {
		case exitCoder:
			exitStatus.Status = v.ExitCode()
		default:
			exitStatus.Status = -1
			exitStatus.Err = err.Error()
		}
	}

	// if argsin.HangElevated
	if ret {
		log.Println("waiting, press enter")
		os.Stdin.Read(make([]byte, 1))
	}

	// notify unelevated that the child has exited, then exit elevated
	gwr.Encode(wrap{V: exitStatus})
}

const piperoot = `\\.\pipe\`

type sig struct {
	Signal syscall.Signal
}

type args struct {
	Args             []string
	Logfile          string
	Verbose          bool
	LogRawOut        bool
	LogRawIn         bool
	DontHideElevated bool
	HangElevated     bool
	Cwd              string
	Env              []string
}
type exit struct {
	Status int
	Err    string
}
type elevatedLog struct {
	Log []byte
}
type conSize struct {
	X, Y int
}

type wrap struct {
	V interface{}
}

type exitCoder interface {
	ExitCode() int
}

func init() {
	gob.Register(sig{})
	gob.Register(args{})
	gob.Register(exit{})
	gob.Register(elevatedLog{})
	gob.Register(conSize{})
}

type elog struct {
	*gob.Encoder
}

func (e *elog) Write(buf []byte) (n int, err error) {
	return len(buf), e.Encode(wrap{V: elevatedLog{Log: buf}})
}

func elevatedLogger(gwr *gob.Encoder) io.Writer {
	return &elog{gwr}
}

type uehlog struct {
	Prefix string
}

func (u *uehlog) Write(buf []byte) (n int, err error) {
	log.Printf("[%s] %s", u.Prefix, hex.EncodeToString(buf))
	return len(buf), nil
}

func unelevatedHexLogger(prefix string) io.Writer {
	return &uehlog{prefix}
}
