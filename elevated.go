//go:build windows

/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// elevated.go: elevated logic to create a process connected to a conpty and feed back to the unelvated side

package main

import (
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
	"unsafe"

	"github.com/Microsoft/go-winio"
	"golang.org/x/sys/windows"
)

func elevated() (bool, error) {
	pid := *unelevatedPid
	var hPC windows.Handle // contpy handle/hpcon

	// connect named pipe for oob(unelevated->elevated)
	coobToElevated, err := winio.DialPipe(fmt.Sprintf("%s%s.%d", piperoot, "loobToElevated", pid), nil)
	if err != nil {
		return true, fmt.Errorf("failed DialPipe for oobToElevated, %w", err)
	}
	defer coobToElevated.Close()

	grd := gob.NewDecoder(coobToElevated)
	var argsin args
	var consize atomic.Value
	var conptyExists = false
	var pi windows.ProcessInformation
	var childExists = false
	var argswg, consizewg sync.WaitGroup
	argswg.Add(1)
	consizewg.Add(1)
	var argsOnce, consizeOnce sync.Once
	go func() { // oobToElevated handler
		// handle args/signals/consoleSizeChanges from unelevated
		var w wrap
		for {
			err = grd.Decode(&w)
			if err != nil && err != winio.ErrFileClosed {
				log.Println(fmt.Errorf("failed Decode for oobToElevated, %w", err))
				return
			}
			switch v := w.V.(type) {
			case args:
				argsOnce.Do(func() {
					argsin = v
					argswg.Done()
				})
			case sig:
				// Ctrl-C is handled by being ignored in Unelevated, and the bytes being passed through stdin to the conpty
				log.Println(v)
				// Ctrl-BREAK will show up as an os.Interrupt
				if childExists {
					switch v.Signal {
					case os.Interrupt:
						log.Println(pi.ProcessId, sendCtrlBreak(int(pi.ProcessId)))
					}
				}
			case conSize:
				consize.Store(v)
				consizeOnce.Do(func() {
					consizewg.Done()
				})
				// only try to resize conpty if it's initialized
				if conptyExists {
					ret, _, errno := syscall.SyscallN(resizePseudoConsole, uintptr(hPC), coordToUintptr(uint16(v.X), uint16(v.Y)))
					if ret != uintptr(windows.S_OK) {
						if errno != 0 {
							log.Println(fmt.Errorf("failed ResizePseudoConsole, ret: %d, err: %w", ret, errno))
							return
						}
						log.Println(fmt.Errorf("failed ResizePseudoConsole, ret: %d, err: %w", ret, syscall.EINVAL))
						return
					}
				}
			default:
				log.Printf("got unhandled message: %T, %+v", v, v)
			}
		}
	}()

	// block until args shows up, timeout after 3sec
	waitCh := make(chan struct{})
	go func() {
		argswg.Wait()
		waitCh <- struct{}{}
	}()
	select {
	case <-waitCh:
		break
	case <-time.After(3 * time.Second):
		return true, fmt.Errorf("timeout waiting for args from unelevated")
	}

	if argsin.Verbose {
		log.Println("got argsin", argsin)
	}

	// hide the elevated console window unless we were told not to
	if !argsin.DontHideElevated {
		if argsin.Verbose {
			log.Println("hiding console")
		}

		hwnd, _, errno := syscall.SyscallN(getConsoleWindow)
		if errno != 0 {
			log.Println("GetConsoleWindow errno,", uintptr(errno), errno.Error())
		}
		if hwnd != 0 {
			_, _, errno := syscall.SyscallN(showWindow, hwnd, windows.SW_HIDE)
			if errno != 0 {
				log.Println("ShowWindow errno,", uintptr(errno), errno.Error())
			}
		}
	}

	// fail if no command line was given
	if len(argsin.Args) == 0 {
		return argsin.HangElevated, fmt.Errorf("no args?")
	}

	// connect stdin/stdout pipes
	if argsin.Verbose {
		log.Println("connecting stdin/out pipes")
	}

	cstdin, err := winio.DialPipe(fmt.Sprintf("%s%s.%d", piperoot, "lstdin", pid), nil)
	if err != nil {
		return argsin.HangElevated, fmt.Errorf("failed DialPipe for stdin, %w", err)
	}
	defer cstdin.Close()
	cstdout, err := winio.DialPipe(fmt.Sprintf("%s%s.%d", piperoot, "lstdout", pid), nil)
	if err != nil {
		return argsin.HangElevated, fmt.Errorf("failed DialPipe for stdout, %w", err)
	}
	defer cstdout.Close()

	// fail without conpty
	if createPseudoConsole == 0 || resizePseudoConsole == 0 || closePseudoConsole == 0 {
		// return argsin.HangElevated, fallback()
		return argsin.HangElevated, fmt.Errorf("PseudoConsole functions not found, old version of windows?")
	}

	if argsin.Verbose {
		log.Println("modern windows with conpty detected")
		log.Println("opening process std io pipes")
	}

	// create pipes for conpty
	pstdinr, pstdinw, err := os.Pipe()
	if err != nil {
		return argsin.HangElevated, fmt.Errorf("failed Pipe() for pstdin, %w", err)
	}
	defer pstdinr.Close()
	defer pstdinw.Close()
	pstdoutr, pstdoutw, err := os.Pipe()
	if err != nil {
		return argsin.HangElevated, fmt.Errorf("failed Pipe() for pstdout, %w", err)
	}
	defer pstdoutr.Close()
	defer pstdoutw.Close()

	// irdOut := io.TeeReader(pstdoutr, hex.NewEncoder(os.Stdout))

	// copy from unelvated stdin to child stdin
	go func() {
		var iwrIn io.Writer
		iwrIn = pstdinw
		if argsin.Verbose {
			// also write to elevated console window, for debugging
			iwrIn = io.MultiWriter(os.Stdout, pstdinw)
		}

		_, err := io.Copy(iwrIn, cstdin)
		if err != nil && err != winio.ErrFileClosed {
			log.Println(fmt.Errorf("failed Copy for pstdinw, %w", err))
		}
	}()
	// copy from child stdout to unelvated stdout
	go func() {
		_, err := io.Copy(cstdout, pstdoutr)
		if err != nil && err != winio.ErrFileClosed {
			log.Println(fmt.Errorf("failed Copy for pstdoutr, %w", err))
		}
		// log.Println("stdout done")
	}()

	if argsin.Verbose {
		log.Println("creating conpty")
	}

	// block until a console size message shows up, timeout after 3sec
	waitCh = make(chan struct{})
	go func() {
		consizewg.Wait()
		waitCh <- struct{}{}
	}()
	select {
	case <-waitCh:
		break
	case <-time.After(3 * time.Second):
		return argsin.HangElevated, fmt.Errorf("timeout waiting for consize from unelevated")
	}

	consz := consize.Load().(conSize)

	// Create conpty, connecting it to the anonymous pipes, which are, in turn, connected to the named pipes to/from unelevated
	ret, _, errno := syscall.SyscallN(createPseudoConsole, coordToUintptr(uint16(consz.X), uint16(consz.Y)), pstdinr.Fd(), pstdoutw.Fd(), 0, uintptr(unsafe.Pointer(&hPC)))
	if ret != uintptr(windows.S_OK) {
		if errno != 0 {
			return argsin.HangElevated, fmt.Errorf("failed CreatePsuedoConsole, ret: %d, err: %w", ret, errno)
		}
		return argsin.HangElevated, fmt.Errorf("failed CreatePsuedoConsole, ret: %d, err: %w", ret, syscall.EINVAL)
	}
	defer syscall.SyscallN(closePseudoConsole, uintptr(hPC))

	conptyExists = true

	if argsin.Verbose {
		log.Println("creating startupinfoex")
	}

	// Prepare startupinfoex structure
	var siEx startupInfoEx
	siEx.Cb = uint32(unsafe.Sizeof(startupInfoEx{}))
	var size uintptr // seems like sizeof(uintptr_t) == sizeof(size_t) on the windows 32 and 64 bit systems I'm targeting.
	// Pass a NULL byte pointer to ask the syscall for the correct size of the startupInfoEx.lpAttributeList element
	syscall.SyscallN(initializeProcThreadAttributeList, 0, 1, 0, uintptr(unsafe.Pointer(&size)))
	// ignore error here because it's intentional
	// if ret == 0 {
	// 	if errno != 0 {
	// 		return argsin.HangElevated, fmt.Errorf("failed InitializeProcThreadAttributeList, err: %w", errno)
	// 	}
	// 	return argsin.HangElevated, fmt.Errorf("failed InitializeProcThreadAttributeList, err: %w", syscall.EINVAL)
	// }
	if size == 0 {
		return argsin.HangElevated, fmt.Errorf("failed to get size from InitializeProcThreadAttributeList")
	}
	l := make([]byte, size)
	siEx.lpAttributeList = &l[0]
	// populate lpAttributeList
	ret, _, errno = syscall.SyscallN(initializeProcThreadAttributeList, uintptr(unsafe.Pointer(&l[0])), 1, 0, uintptr(unsafe.Pointer(&size)))
	if ret == 0 {
		if errno != 0 {
			return argsin.HangElevated, fmt.Errorf("failed InitializeProcThreadAttributeList, err: %w", errno)
		}
		return argsin.HangElevated, fmt.Errorf("failed InitializeProcThreadAttributeList, err: %w", syscall.EINVAL)
	}
	// set the lpAttributeList to enable use of the conpty created earlier
	ret, _, errno = syscall.SyscallN(updateProcThreadAttribute, uintptr(unsafe.Pointer(&l[0])), 0, PROC_THREAD_ATTRIBUTE_PSEUDOCONSOLE, uintptr(hPC), unsafe.Sizeof(hPC), 0, 0)
	if ret == 0 {
		if errno != 0 {
			return argsin.HangElevated, fmt.Errorf("failed UpdateProcThreadAttribute, err: %w", errno)
		}
		return argsin.HangElevated, fmt.Errorf("failed UpdateProcThreadAttribute, err: %w", syscall.EINVAL)
	}

	// Prepare command line, add quotes to all args that don't contain quotes already
	// This is not perfect, but I wanted to avoid making a thing that adds a layer of escapes on top of an n-layer ecaped string
	a := make([]string, len(argsin.Args))
	for i, v := range argsin.Args {
		if !strings.Contains(a[i], `"`) {
			a[i] = `"` + v + `"`
		} else {
			a[i] = v
		}
	}
	sargs := strings.Join(a, " ")

	if argsin.Verbose {
		log.Println("running:", sargs)
		log.Println("creating child")
	}

	// CreateProcess to run the child process, attached to conpty
	// TODO: switch to UTF16PtrFromString
	var penv, pcwd *uint16
	if len(argsin.Env) != 0 {
		penv = stringsToEnvironUTF16Ptr(argsin.Env)
	}
	if argsin.Cwd != "" {
		pcwd = windows.StringToUTF16Ptr(argsin.Cwd)
	}
	err = createProcess(nil, windows.StringToUTF16Ptr(sargs), nil, nil, false, windows.EXTENDED_STARTUPINFO_PRESENT|windows.CREATE_UNICODE_ENVIRONMENT, penv, pcwd, &siEx, &pi)
	if err != nil {
		return argsin.HangElevated, fmt.Errorf("failed CreateProcessW, err: %w", err)
	}
	defer windows.CloseHandle(pi.Thread)
	defer windows.CloseHandle(pi.Process)
	childExists = true

	if argsin.Verbose {
		log.Println("waiting for child to exit", argsin)
	}

	// block for child exit
	s, e := syscall.WaitForSingleObject(syscall.Handle(pi.Thread), syscall.INFINITE)
	childExists = false
	switch s {
	case syscall.WAIT_OBJECT_0:
		break
	case syscall.WAIT_FAILED:
		return argsin.HangElevated, fmt.Errorf("failed WaitForSingleObject, err: %w", os.NewSyscallError("WaitForSingleObject", e))
	default:
		return argsin.HangElevated, fmt.Errorf("failed WaitForSingleObject, err: %w", errors.New("os: unexpected result from WaitForSingleObject"))
	}

	// Get child run information
	var ec uint32
	e = syscall.GetExitCodeProcess(syscall.Handle(pi.Process), &ec)
	if e != nil {
		return argsin.HangElevated, fmt.Errorf("failed GetExitCodeProcess, err: %w", os.NewSyscallError("GetExitCodeProcess", e))
	}
	var u syscall.Rusage
	e = syscall.GetProcessTimes(syscall.Handle(pi.Process), &u.CreationTime, &u.ExitTime, &u.KernelTime, &u.UserTime)
	if e != nil {
		return argsin.HangElevated, fmt.Errorf("failed GetProcessTimes, err: %w", os.NewSyscallError("GetProcessTimes", e))
	}
	runtime.KeepAlive(l) // because I'm paranoid.  I don't want l to be garbage collected until after here, just in case its needed for something inside windows system calls.

	if argsin.Verbose {
		log.Println("returns:", ec)
		// log.Printf("times: %+v\n", u)
		runtime := time.Duration(u.ExitTime.Nanoseconds()) - time.Duration(u.CreationTime.Nanoseconds())
		ktime := (time.Duration(u.KernelTime.HighDateTime)<<32 + time.Duration(u.KernelTime.LowDateTime)) * 100
		utime := (time.Duration(u.UserTime.HighDateTime)<<32 + time.Duration(u.UserTime.LowDateTime)) * 100
		log.Println("runtime:", runtime, "kernel time:", ktime, "user time:", utime)
	}

	conptyExists = false

	// sleep (hopefully) long enough to let the stdout pipe empty into the unelevated process
	time.Sleep(time.Second)

	err = nil
	if ec != 0 {
		ex := new(exitCode)
		*ex = exitCode(ec)
		err = ex
	}

	return argsin.HangElevated, err
}

type exitCode int

func (e *exitCode) ExitCode() int {
	return int(*e)
}
func (e *exitCode) Error() string {
	return fmt.Sprintf("non-zero return: %d", e)
}
