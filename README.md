win-sudo
======

## About
Microsoft really doesn't like an unelevated terminal window to run an elevated cmd or powershell.  There are good reasons for this, and cheif among them is the fact that any unelevated process can send keystrokes to any unelevated window -- a priveledge escalation exploit if an unelevated window could be running an elevated shell.

This project aims to let this happen anyway for the convienience of not having to open an elevated window just for a quick elevated powershell session.  A UAC prompt will appear when the elevated process is launched.

## Usage
`win-sudo powershell`

There are various flags:
* TODO

## How it works
win-sudo has two modes controlled by arguments.  The user should not try to pass a `-unelevatedPid` argument - it would likely just fail since the process wouldn't be elevated, but it also might interfere with a different win-sudo session.

Nominally:
1. When launched normally, win-sudo(_Unelevated_) first relaunches itself elevated(_Elevated_), with adjusted arguments.  This will show a UAC prompt.  
2. _Elevated_ sets up a winpty for connecting stdin/out/err.
3. The two win-sudo processes communicate over named pipes with gob-encoded messages.  _Unelevated_ sends the command+environment to _Elevated_.
4. _Elevated_ exec's the command+environment, which will also be elevated.  This is connected to the winpty.  This elevated child process will be called _Child_.
5. _Elevated_ sends any stdout or stderr over the named pipes to _Unelevated_.  And _Unelevated_ sends any stdin over the named pipes to _Elevated_.

There are various edge cases:
* Signals:
  * _Unelevated_ needs to handle Ctrl-C and Ctrl-Break and inform _Elevated_ so _Elevated_ can resend the signal to _Child_.
* Console window:
  * _Elevated_ has a console window, which should normally be hidden, but can be used for debugging.  It may show breifly before _Elevated_ hides it.
